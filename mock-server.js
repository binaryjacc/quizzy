import express from 'express'
import mockApiRouter from './mock-api/api.js'

const app = express()

app.use(express.json())

app.use('/api', mockApiRouter)

app.use('/api/*', (req, res) => res.status(404).send("Not found."))

app.use(express.static('public'))

app.get('/*', (req, res) => res.sendFile(process.cwd() + '/public/index.html'))

console.log(`Listening on port 3000 ${new Date()}`)

app.listen(3000)
