import express from 'express'
import apiRouter from './src/api.js'

const app = express()

app.use(express.json())

app.use('/api', apiRouter)

app.use('/api/*', (req, res) => res.status(404).send("Not found."))

app.use(express.static('public'))

app.get('/*', (req, res) => res.sendFile(process.cwd() + '/public/index.html'))

export default app
