import express from 'express'
import questionHighlight from './questions-highlight.json' assert {type: 'json'}
import topics from './topics.json' assert {type: 'json'}
import topicsId from './topicsId.json' assert {type: 'json'}
import collectionsId from './collectionsId.json' assert {type: 'json'}
import collectionsIdAnswers from './collectionsIdAnswers.json' assert {type: 'json'}



const router = express.Router()

router.get('/', (req, res) => res.json({message: 'API ROOT'}))

router.get('/questions/highlight', (req, res) => res.json(questionHighlight))

router.get('/topics', (req, res) => res.json(topics))

router.get('/topics/:id', (req, res) => res.json(topicsId))

router.get('/collections/:id', (req, res) => res.json(collectionsId))

router.post('/collections/:id/answers', (req, res) => res.json(collectionsIdAnswers))

router.post('/users', (req, res) => res.json({}))

router.post('/login', (req, res) => res.json({}))

//router.get('/logout', (req, res) => res.json({}))

export default router
