import BaseElement from '../BaseElement.js'
import {ROUTE_EVENT_NAME} from './router.js'

customElements.define(
  'rp-route-page',
  class extends BaseElement {
    beforeRender() {
      document.addEventListener(ROUTE_EVENT_NAME, (e) => {
        this.innerHTML = `<${e.detail.component} />`
      })
    }
  },
)
