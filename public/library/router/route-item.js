import BaseElement from '../BaseElement.js'
import {navigateEvent} from './router.js'

customElements.define(
  'rp-route-item',
  class extends BaseElement {
    render() {
      return /*html*/ `
        <a href="${this.getAttribute('path')}">${this.innerHTML}</a>
      `
    }

    afterRender() {
      this.addEventListener('click', (e) => {
        e.preventDefault()
        this.dispatchEvent(
          navigateEvent({path: this.getAttribute('path'), component: this.getAttribute('component')}),
        )
      })
    }
  },
)
