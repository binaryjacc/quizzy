import BaseElement from '../BaseElement.js'

export const ROUTE_EVENT_NAME = 'route'
export const NAVIGATE_EVENT_NAME = 'navigate'

export const navigateEvent = (detail) => new CustomEvent(NAVIGATE_EVENT_NAME, {detail, composed: true, bubbles: true})

export const routeEvent = (detail) => new CustomEvent(ROUTE_EVENT_NAME, {detail, composed: true, bubbles: true})

customElements.define(
  'rp-router',
  class extends BaseElement {
    navigate(routeItem) {
      window.history.pushState(routeItem, undefined, routeItem.path)

      this.dispatchEvent(routeEvent(routeItem))
    }

    afterRender() {
      this.addEventListener(NAVIGATE_EVENT_NAME, (e) => {
        this.navigate(e.detail)
      })

      setTimeout(() => {
        this.querySelectorAll('rp-route-item').forEach(routeItem => {
          if (routeItem.getAttribute('path') === window.location.pathname) {
            this.navigate({path: window.location.pathname, component: routeItem.getAttribute('component')})
          }
        })
      })
      
      window.addEventListener('popstate', (e) => {
        this.dispatchEvent(routeEvent(e.state))
      })
    }
  },
)
