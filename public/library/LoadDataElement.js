import BaseElement from "./BaseElement.js"

export default class extends BaseElement {
  data = {}

  async connectedCallback() {
    await this.loadData()
    await this.beforeRender?.()
    this.changeContent()
    await this.afterRender?.()
    this.dispatchEvent(new CustomEvent('element-loaded', {composed: true, bubbles: true}))
  }

  async loadData() {
    const response = await fetch(this.getAttribute('src'))
    this.data = await response.json()
    this.dispatchEvent(new CustomEvent('data-loaded', {composed: true, bubbles: true}))
  }
}
