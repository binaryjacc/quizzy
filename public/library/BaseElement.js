export default class extends HTMLElement {
  async connectedCallback() {
    await this.beforeRender?.()
    this.changeContent()
    await this.afterRender?.()
    this.dispatchEvent(new CustomEvent('element-loaded', {composed: true, bubbles: true}))
  }

  attributeChangedCallback() {
    this.innerHTML = ''
    this.changeContent()
  }

  changeContent() {
    if (! this.render) {
      return
    }

    this.innerHTML = this.render()
  }
}
