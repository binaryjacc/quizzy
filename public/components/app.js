import BaseElement from '../library/BaseElement.js'
import '../library/router/router.js'
import '../library/router/page.js'
import './pages/home.js'
import './pages/about.js'
import './header.js'

customElements.define(
  'rp-app',
  class extends BaseElement {
    render() {
      return /*html*/ `
        <rp-router>
          <rp-header></rp-header>
          <rp-route-page><rp-route-page>    
        </rp-router>
      `
    }
  },
)
