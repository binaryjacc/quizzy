import LoadDataElement from '../../../library/LoadDataElement.js'

customElements.define(
  'rp-home-topics',
  class extends LoadDataElement {
    render() {
      return /*html*/ `
        <div class="pure-g">
          ${this.data
            .map((item) => {
              return /*html*/ `
              <div class="pure-u-1 pure-u-md-1-3">
                <div style="border: 1px solid #ddd; margin: 0 0.5em 2em; background: white">
                  <rp-route-item path="/topics/${item.slug}" component="rp-topics">
                    <img width="100%" src="${item.image}" alt="${item.title}">
                    <p style="padding: 10px">${item.title}</p>
                  </rp-route-item>
                </div>
              </div>`
            })
            .join('')}
        </div>
      `
    }
  },
)
