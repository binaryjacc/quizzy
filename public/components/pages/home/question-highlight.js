import LoadDataElement from '../../../library/LoadDataElement.js'

customElements.define(
  'rp-question-highlight',
  class extends LoadDataElement {
    render() {
      return /*html*/ `
        <h3 style="color: white">${this.data.title}</h3>
        <div class="pure-g">
          ${this.data.options.map(option => {
            return /*html*/ `
              <div class="pure-u-1 pure-u-md-1-5">
                <label style="width: 95%" class="button-secondary button-xlarge pure-button"><input name="response" type="radio" value="${option.correct}"> ${option.title}</label>
              </div>`
          }).join('')}
        </div>
      `
    }
  },
)