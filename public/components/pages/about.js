import BaseElement from '../../library/BaseElement.js'

customElements.define(
  'rp-about',
  class extends BaseElement {
    render() {
      return /*html*/ `
        <h2>About</h2>
      `
    }
  },
)