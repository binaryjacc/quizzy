import BaseElement from '../../library/BaseElement.js'
import './home/home-topics.js'
import './home/question-highlight.js'
import {appUrl} from '../../config.js'

customElements.define(
  'rp-home',
  class extends BaseElement {
    render() {
      return /*html*/ `
        <div style="margin: 0 auto;">
          <div style="background: rgb(0, 34, 85); margin-bottom: 10px; height: 200px">
            <div class="pure-g" style="max-width: 980px; margin: 0 auto;">
              <div class="pure-u-1">
                <rp-question-highlight src="${appUrl}/questions/highlight"></rp-question-highlight>
              </div>
            </div>
          </div>
          <div class="pure-g" style="max-width: 980px; margin: 0 auto;">
            <div class="pure-u-1">
              <rp-home-topics src="${appUrl}/topics"></rp-home-topics>
            </div>
          </div>
        </div>
      `
    }
  },
)
