import BaseElement from '../../library/BaseElement.js'
import '../library/router/route-item.js'

customElements.define(
  'rp-header',
  class extends BaseElement {
    render() {
      return /*html*/ `
        <div class="pure-menu pure-menu-horizontal">
          <rp-route-item class="pure-menu-heading" path="/" component="rp-home">Quizzy</rp-route-item>
          <ul class="pure-menu-list">
              <li class="pure-menu-item">
                <rp-route-item class="pure-menu-link" path="/" component="rp-home">Home</rp-route-item>
              </li>
              <li class="pure-menu-item">
                <rp-route-item class="pure-menu-link" path="/about" component="rp-about">About</rp-route-item>
              </li>
          </ul>
        </div>
      `
    }
  },
)

