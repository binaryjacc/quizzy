import it from 'node:test'
import assert from 'node:assert'
import request from 'supertest'
import app from '../../app.js'

it('checks the response body for the root api', async () => {
  const response = await request(app)
    .get('/api')

  assert.equal(response.statusCode, 200)
  assert.equal(response.body.message, 'API ROOT')
})