import 'global-jsdom/register'
import '../../../../fetch-polyfill.js'
import {it, before} from 'node:test'
import assert from 'node:assert'
import questionHighlight from '../../../../../mock-api/questions-highlight.json' assert {type: 'json'}
import '../../../../../public/components/pages/home/question-highlight.js'

let el

before(async () => {
  el = document.createElement('rp-question-highlight')
  el.setAttribute('src', `data:application/json,${encodeURIComponent(JSON.stringify(questionHighlight))}`)
  document.body.append(el)

  await new Promise((resolve) => el.addEventListener('element-loaded', resolve))
})

it('prints question title', async () => {
  assert.match(el.innerHTML, new RegExp(`${questionHighlight.title}`))
})

it('prints question options', () => {
  const options = el.querySelectorAll('label')

  assert.match(options[0].innerHTML, new RegExp(`${questionHighlight.options[0].title}`))
  assert.match(options[1].innerHTML, new RegExp(`${questionHighlight.options[1].title}`))
  assert.match(options[2].innerHTML, new RegExp(`${questionHighlight.options[2].title}`))
})
