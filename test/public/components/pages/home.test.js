import 'global-jsdom/register'
import '../../../fetch-polyfill.js'
import {it, before} from 'node:test'
import assert from 'node:assert'
import nock from 'nock'
import questionHighlight from '../../../../mock-api/questions-highlight.json' assert {type: 'json'}
import '../../../../public/components/pages/home.js'

let el

before(async () => {
  nock.disableNetConnect()
  nock('http://localhost:3000').get('/api/questions/highlight').reply(200, questionHighlight)

  el = document.createElement('rp-home')
  document.body.append(el)
  
  await new Promise((resolve) => el.addEventListener('element-loaded', resolve))
})

it('prints page title', async () => {
  assert.ok(el.innerHTML)
})

it('renders highlight question', async () => {
  const elQuestion = el.querySelector('rp-question-highlight')
  
  await new Promise((resolve) => elQuestion.addEventListener('element-loaded', resolve))

  assert.ok(elQuestion.innerHTML)
  assert.match(elQuestion.innerHTML, new RegExp(`${questionHighlight.title}`))
})
