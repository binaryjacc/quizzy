import 'global-jsdom/register'
import {it, before} from 'node:test'
import assert from 'node:assert'
import '../../../../public/components/pages/about.js'

let el

before(() => {
  el = document.createElement('rp-about')
  document.body.append(el)
})

it('prints page title', async () => {
  assert.match(el.innerHTML, /About/)
})
