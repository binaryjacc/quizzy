import 'global-jsdom/register'
import {it, before} from 'node:test'
import assert from 'node:assert'
import '../../../../public/library/router/router.js'
import '../../../../public/library/router/route-item.js'
import '../../../../public/library/router/page.js'
import BaseElement from '../../../../public/library/BaseElement.js'

let el

before(() => {
  el = document.createElement('rp-router')
  document.body.append(el)

  customElements.define(
    'rp-test',
    class extends BaseElement {
      render() {
        'test'
      }
    },
  )
})

it('has not action if there is no route-item and page', async () => {
  assert.equal(el.innerHTML, '')
})

it('changes page content base on the item', async () => {
  const item = document.createElement('rp-route-item')
  item.setAttribute('path', '/test')
  item.setAttribute('component', 'rp-test')

  const page = document.createElement('rp-route-page')

  el.append(item)
  el.append(page)

  await Promise.resolve()

  item.dispatchEvent(new MouseEvent('click'))

  assert.match(page.innerHTML, /test/)
})
