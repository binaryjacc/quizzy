import 'global-jsdom/register'
import {it, before} from 'node:test'
import assert from 'node:assert'
import '../../../../public/library/router/route-item.js'
import {NAVIGATE_EVENT_NAME} from '../../../../public/library/router/router.js'

let el

before(() => {
  el = document.createElement('rp-route-item')
  el.setAttribute('path', '/test')
  el.setAttribute('component', 'rp-test')
  document.body.append(el)
})

it('dispatches navigates event when clink', (_, done) => {
  el.addEventListener(NAVIGATE_EVENT_NAME, (e) => {
    try {
      assert.deepEqual({path: el.getAttribute('path'), component: el.getAttribute('component')}, e.detail)
      done()
    } catch (e) {
      done(e)
    }
  })

  el.dispatchEvent(new MouseEvent('click'))
})
