import 'global-jsdom/register'
import {it, before} from 'node:test'
import assert from 'node:assert'
import '../../../../public/library/router/page.js'
import BaseElement from '../../../../public/library/BaseElement.js'
import {routeEvent} from '../../../../public/library/router/router.js'

let el

before(() => {
  el = document.createElement('rp-route-page')
  document.body.append(el)

  customElements.define(
    'rp-test',
    class extends BaseElement {
      render() {
        'test'
      }
    },
  )
})

it('changes content based on route event', async () => {
  assert.equal(el.innerHTML, '')

  document.dispatchEvent(routeEvent({path: '/test', component: 'rp-test'}))

  assert.match(el.innerHTML, /test/)
})
