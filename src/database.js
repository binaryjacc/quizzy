import {createConnection} from 'mysql'

const connection = createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'quizzy',
})

connection.connect()

export default connection
