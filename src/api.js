import express from 'express'
const router = express.Router()

router.get('/', function (req, res) {
  res.json({message: 'API ROOT'})
})

router.get('/questions/highlight', (req, res) => {})
router.get('/topics', (req, res) => {})
router.get('/topics/:id', (req, res) => {})
router.get('/collection/:id', (req, res) => {})
router.post('/collection/:id/answers', (req, res) => {})

router.post('/users', (req, res) => {})
router.post('/login', (req, res) => {})
router.get('/logout', (req, res) => {})

export default router
